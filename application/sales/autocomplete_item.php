<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/pos.php");

$term = trim(strip_tags($_GET['term']));
$row_set = array();
$pos = new pos();

$data = $pos->autoCompleteItem($term);
foreach ($data[1] as $row) {
	$row['label']=htmlentities(stripslashes($row['item_name']));
	$row['value']=htmlentities(stripslashes($row['item_name']));
	if($row['note'] == '' or $row['note'] == null){
		$row['note']='';
	}else{
		$row['note']='<dd><small> Note : '.$row['note'].'</small></dd>';
	}
	$row['price']='<dd><small> Rp.'.number_format($row['price'],2).' /'.$row['id_unit'].'</small></dd>'.'<small> Ppn : '.number_format($row['tax'],2).' %</small>';

	$row['id_item']=$row['id_item'];
	$row['id_unit']=$row['id_unit'];
	$row_set[] = $row;
}

echo json_encode($row_set);
?>