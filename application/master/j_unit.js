$(document).ready( function () 
{
	money();
	decimal();
	var value = {
		method : "getdata"
	};
	$('#table_unit').DataTable({
		"paging": true,
		"lengthChange": false,
		"searching": true,
		"ordering": true,
		"info": false,
		"responsive": true,
		"autoWidth": false,
		"pageLength": 50,
		"dom": '<"top"f>rtip',
		"ajax": {
			"url": "c_unit.php",
			"type": "POST",
			"data":value,
		},
		"columns": [
		{ "data": "urutan" },
		{ "data": "id_unit" },
		{ "data": "unit_name" },
		{ "data": "tax" },
		{ "data": "note" },
		{ "data": "button" },
		]
	});
	$("#table_unit_filter").addClass("pull-right");
});

$(document).on( "click","#btnadd", function() {
	$(".contentharga").remove();
	$("#modalmasterunit").modal('show');
	newunit();
});
function newunit()
{
	$("#txtidunit").val("*New");
	$("#txtname").val("");
	$("#txttax").val(0);
	$("#txtnote").val("");
	$("#inputcrud").val("N");
	$("#txtunit").change();
	set_focus("#txtname");
}
$(document).on( "click",".btnedit", function() {
	var id_unit = $(this).attr("id_unit");
	var value = {
		id_unit: id_unit,
		method : "get_detail_unit"
	};
	$.ajax(
	{
		url : "c_unit.php",
		type: "POST",
		data : value,
		success: function(data, textStatus, jqXHR)
		{
			var hasil = jQuery.parseJSON(data);
			data = hasil.data;
			$("#inputcrud").val("E");
			$("#txtidunit").val(data.id_unit);
			$("#txtname").val($.trim(data.unit_name));
			$("#txttax").val(addCommas(data.tax));
			$("#txtnote").val($.trim(data.note));
			$("#modalmasterunit").modal('show');
			set_focus("#txtname");
		},
		error: function(jqXHR, textStatus, errorThrown)
		{
		}
	});
});
$(document).on( "click","#btnsaveunit", function() {
	var id_unit = $("#txtidunit").val();
	var unit_name = $("#txtname").val();
	var tax = cleanString($("#txttax").val());
	var note = $("#txtnote").val();
	var crud=$("#inputcrud").val();
	if(crud == 'E'){
		if(id_unit == '' || id_unit== null ){
			$.notify({
				message: "Unit Id invalid"
			},{
				type: 'warning',
				delay: 8000,
			});		
			$("#txtidunit").focus();
			return;
		}	
	}
	if(unit_name == '' || unit_name== null ){
		$.notify({
			message: "Please fill out unit name"
		},{
			type: 'warning',
			delay: 8000,
		});		
		$("#txtname").focus();
		return;
	}

	var value = {
		id_unit: id_unit,
		unit_name: unit_name,
		tax:tax,
		note:note,
		crud:crud,
		method : "save_unit"
	};
	$(this).prop('disabled', true);
	proccess_waiting("#infoproses");
	$.ajax(
	{
		url : "c_unit.php",
		type: "POST",
		data : value,
		success: function(data, textStatus, jqXHR)
		{
			$("#btnsaveunit").prop('disabled', false);
			$("#infoproses").html("");
			var data = jQuery.parseJSON(data);
			if(data.ceksat == 0){
				$.notify(data.error);
			}else{
				if(data.crud == 'N'){
					if(data.result == 1){
						$.notify('Save unit successfuly');
						var table = $('#table_unit').DataTable(); 
						table.ajax.reload( null, false );
						newunit();				
					}else{
						$.notify({
							message: "Error save unit, error :"+data.error
						},{
							type: 'danger',
							delay: 8000,
						});
						set_focus("#txtidunit");
					}
				}else if(data.crud == 'E'){
					if(data.result == 1){
						$.notify('Update unit successfuly');
						var table = $('#table_unit').DataTable(); 
						table.ajax.reload( null, false );
						$("#modalmasterunit").modal("hide");
					}else{
						$.notify({
							message: "Error update unit, error :"+data.error
						},{
							type: 'danger',
							delay: 8000,
						});					
						set_focus("#txtidunit");
					}
				}else{
					$.notify({
						message: "Invalid request"
					},{
						type: 'danger',
						delay: 8000,
					});	
				}
			}
		},
		error: function(jqXHR, textStatus, errorThrown)
		{
			$("#btnsaveunit").prop('disabled', false);
		}
	});
});
$(document).on( "click",".btndelete", function() {
	var id_unit = $(this).attr("id_unit");
	swal({   
		title: "Delete",   
		text: "Delete master unit with id : "+id_unit+" ?",   
		type: "warning",   
		showCancelButton: true,   
		confirmButtonColor: "#DD6B55",   
		confirmButtonText: "Delete",   
		closeOnConfirm: true }, 
		function(){   
			var value = {
				id_unit: id_unit,
				method : "delete_unit"
			};
			$.ajax(
			{
				url : "c_unit.php",
				type: "POST",
				data : value,
				success: function(data, textStatus, jqXHR)
				{
					var data = jQuery.parseJSON(data);
					if(data.result ==1){
						$.notify('Delete unit successfuly');
						var table = $('#table_unit').DataTable(); 
						table.ajax.reload( null, false );
					}else{
						$.notify({
							message: "Error delete unit, error :"+data.error
						},{
							type: 'eror',
							delay: 8000,
						});	
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
				}
			});
		});
});

