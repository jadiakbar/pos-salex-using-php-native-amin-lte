<?php
session_start();
require_once ("../model/dbconn.php");
require_once ("../model/pos.php");
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
{
	$pos = new pos();
	$method=$_POST['method'];
	if($method == 'get_detail_unit')
	{
		$id_unit=$_POST['id_unit'];
		$pos = new pos();
		$data = $pos->getunit($id_unit);
		$array['data'] = $data[1];
		$array['result'] = $data[0];
		echo json_encode($array);
	}
	if($method == 'save_unit')
	{
		$idunit = $_POST['id_unit'];
		$nameunit = $_POST['unit_name'];
		$tax = $_POST['tax'];
		$note = $_POST['note'];
		$crud=$_POST['crud'];
		$pos = new pos();
		if($_POST['crud'] == 'N')
		{
			$array = $pos->saveunit($nameunit,$tax,$note);
			if($array[0] == true)
			{
				$result['id_unit'] = $array[2];
			}
			
		}
		else
		{
			$array = $pos->updateunit($idunit,$nameunit,$tax,$note);
		}
		$result['result'] = $array[0];
		$result['error'] = $array[1];
		$result['crud'] = $_POST['crud'];
		echo json_encode($result);
	}
	
	if($method == 'getdata'){
		$pos = new pos();
		$array = $pos->getListunit();
		$data = $array[1];
		$i=0;
		foreach ($data as $key) {
			$button = '<button  type="submit" id_unit="'.$key['id_unit'].'"  title="Tombol edit unit" class="btn btn-sm btn-primary btnedit "  id="btnedit'.$key['id_unit'].'"  ><i class="fa fa-edit"></i></button> <button  type="submit" id_unit="'.$key['id_unit'].'"  title="Tombol hapus unit" class="btn btn-danger btn-sm btndelete "  id="btndelete'.$key['id_unit'].'"  ><i class="fa fa-trash"></i></button>';

			$data[$i]['tax'] =  number_format($data[$i]['tax'],2);
			$data[$i]['DT_RowId']= $data[$i]['id_unit'];
			$data[$i]['button'] = $button;
			$i++;
		}
		$datax = array('data' => $data);
		echo json_encode($datax);
	}
	if($method == 'delete_unit'){
		$id_unit=$_POST['id_unit'];
		$pos = new pos();
		$array = $pos->deleteunit($id_unit);
		$data['result'] = $array[0];
		$data['error'] = $array[1];
		echo json_encode($data);
	}
	
} else {
	exit('No direct access allowed.');
}